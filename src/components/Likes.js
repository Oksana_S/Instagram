import React from 'react';
import LikeItem from './LikeItem';

const likes = [  'Vlad',    'Nik',    'Mark' ];



export default class Likes extends React.Component {

    constructor(params) {
        super(params);
        this.state = {
            inputValue: '',
            likes: likes
        };
    }

    handleChange = e => {
        this.setState({inputValue: e.target.value})
    }

    handleClick = () => {
        this.setState ({
            likes:[...this.state.likes, this.state.inputValue]
        })

    }

    render() {
        const list = this.state.likes.map(name => <LikeItem key={name} name={name}/> );
        console.log(list);
        return (
          <div>
              <input type="text" value={this.state.inputValue} onChange={this.handleChange}/>
            <button onClick={this.handleClick}>Add</button>
            <ul>{list}</ul>
    </div>
    )
    }

}

